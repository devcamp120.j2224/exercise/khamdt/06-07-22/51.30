package com.devcamp.j02_javabasic.s30;

public class Customer {
    byte myByte; // byte
    short myShortNumber; // short
    long myLongNumber; // long
    double myDoubleNumber; 
    int myIntNumber; // integer (whole number)
    float myFloatNumber; // floating point number
    char myLetter; // character
    boolean myBool; // boolean

        public Customer(){
            myByte = 127; // byte
            myShortNumber = 2022 ; // short
            myLongNumber = 2021; // long
            myDoubleNumber = 123.4; // double
            myIntNumber = 5; // integer (whole number)
            myFloatNumber = 9.99f; // floating point number
            myLetter = 'P'; // character
            myBool = false; // boolean
        }
        public Customer(byte byteNum, short shortNum, long longNum, double doubleNum, int intNum, float floatNum, char letter, boolean bool ){
            myByte = byteNum;
            myShortNumber = shortNum;
            myLongNumber = longNum;
            myDoubleNumber = doubleNum;
            myIntNumber = intNum;
            myLetter = letter;
            myBool = bool;
        }

    public static void main(String[] args){
        Customer customer = new Customer();
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myDoubleNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);
        System.out.println("Run 1: ");
        byte myByte = 10;
        short myShort = 2001;
        customer = new Customer(myByte, myShort, 7001, 671.9, 231, 71.99f, 'A', false );
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myDoubleNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);
        System.out.println("Run 2: ");
        myByte = 20;
        myShort = 2002;
        customer = new Customer(myByte, myShort, 7002, 672.9, 232, 72.99f, 'B', true );
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myDoubleNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);
        System.out.println("Run 3: ");
        myByte = 30;
        myShort = 2003;
        customer = new Customer(myByte, myShort, 7003, 673.9, 233, 73.99f, 'C', false);
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myDoubleNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);
        System.out.println("Run 4: ");
        myByte = 30;
        myShort = 2003;
        customer = new Customer(myByte, myShort, 7004, 674.9, 234, 74.99f, 'D', false);
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myDoubleNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);
        System.out.println("Run 5: ");
        myByte = 30;
        myShort = 2003;
        customer = new Customer(myByte, myShort, 7014, 644.9, 239, 75.99f, 'E', true);
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myDoubleNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);
    
    }
}